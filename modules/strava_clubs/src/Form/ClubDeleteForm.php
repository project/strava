<?php

namespace Drupal\strava_clubs\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Strava club entities.
 *
 * @ingroup strava
 */
class ClubDeleteForm extends ContentEntityDeleteForm {


}
