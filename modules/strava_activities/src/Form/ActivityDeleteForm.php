<?php

namespace Drupal\strava_activities\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Strava Activity entities.
 *
 * @ingroup strava
 */
class ActivityDeleteForm extends ContentEntityDeleteForm {


}
