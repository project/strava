<?php

namespace Drupal\strava_activities;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Strava activity entity.
 *
 * @see \Drupal\strava_activities\Entity\Activity.
 */
class ActivityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\strava_activities\Entity\ActivityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished strava activity entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published strava activity entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit strava activity entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete strava activity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add strava activity entities');
  }

}
