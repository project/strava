<?php

namespace Drupal\strava_athletes;

use Drupal\Core\Link;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Strava athlete entities.
 *
 * @ingroup strava
 */
class AthleteListBuilder extends EntityListBuilder {

  /**
   * @var int
   */
  protected $limit = 25;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Athlete ID');
    $header['label'] = $this->t('Label');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\strava_athletes\Entity\Athlete */
    $row['id'] = $entity->id();
    $row['label'] = Link::fromTextAndUrl($entity->label(), new Url(
      'entity.strava_athlete.edit_form', [
        'strava_athlete' => $entity->id(),
      ]
    ));
    return $row + parent::buildRow($entity);
  }

}
