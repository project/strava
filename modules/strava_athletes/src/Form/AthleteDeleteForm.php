<?php

namespace Drupal\strava_athletes\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Strava athlete entities.
 *
 * @ingroup strava
 */
class AthleteDeleteForm extends ContentEntityDeleteForm {


}
