<?php

namespace Drupal\strava_athletes\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Strava athlete entities.
 */
class AthleteViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['strava_athlete']['table']['base'] = [
      'field' => 'id',
      'title' => $this->t('Strava Athlete'),
      'help' => $this->t('The Athlete ID.'),
    ];

    return $data;
  }

}
